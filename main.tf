terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 1.1.0"
}

#Global variables
locals {
  email = "mail@idoshalev.ca"
  bucket = "notification-dev-project"
  domain = "mail.idoshalev.ca"
}

# Working in us-east-1
provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

# Creating s3 bucket named notification-dev-project with enabled logging
resource "aws_s3_bucket" "log_bucket" {
  bucket = local.bucket
  tags = {
    Name = "Notifications"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket" "b" {
  bucket = local.bucket
  acl = "private"

  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "log/"
  }
}

# Create lambda policy and role
resource "aws_iam_role_policy" "notify_lambda_policy" {
  name = "notify_lambda_policy"
  role = aws_iam_role.notify_lambda_role.id

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "Stmt1650314745499",
        "Action": "logs:*",
        "Effect": "Allow",
        "Resource": "*"
      },
      {
          "Effect": "Allow",
          "Action": [
              "s3:PutObject",
              "s3:GetObject"
          ],
          "Resource": [
              "arn:aws:s3:::${local.bucket}/*"
          ]
      },
      {
          "Effect": "Allow",
          "Action": [
              "ses:SendEmail",
              "ses:SendRawEmail"
          ],
          "Resource": "*"
      },
      {
            "Effect": "Allow",
            "Action": [
                "sns:Publish"
            ],
            "Resource": "*"
        }
    ]
  })
}

resource "aws_iam_role" "notify_lambda_role" {
  name = "notify_lambda_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

# Create a lambda function to ingest the files in notification-dev-project bucket
data "archive_file" "notification" {
  type        = "zip"
  source_file = "notifications.py"
  output_path = "outputs/notifications.zip"
}

resource "aws_lambda_function" "notification_lambda" {
  filename      = "outputs/notifications.zip"
  function_name = "notify"
  role          = aws_iam_role.notify_lambda_role.arn
  handler       = "notifications.notify"

  #source_code_hash = filebase64sha256("lambda_function_payload.zip")

  runtime = "python3.9"
}

# Trigger the lambda function upon a file upload to notification-dev-project bucket
resource "aws_s3_bucket_notification" "aws-lambda-trigger" {
  bucket = local.bucket
  lambda_function {
    lambda_function_arn = aws_lambda_function.notification_lambda.arn
    events              = ["s3:ObjectCreated:*"]

  }
}
resource "aws_lambda_permission" "notify" {
  statement_id  = "AllowS3Invoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.notification_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${local.bucket}"
}

# Configure SES
resource "aws_ses_email_identity" "notify-ses" {
  email = local.email
}

resource "aws_ses_domain_mail_from" "notify-ses" {
  domain = aws_ses_email_identity.notify-ses.email
  mail_from_domain = local.domain
}
