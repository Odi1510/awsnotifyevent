import json
import urllib.parse
import boto3
from botocore.exceptions import ClientError

s3 = boto3.client('s3')
# The sender's email address
SENDER = "mail@idoshalev.ca"
# The AWS Region
AWS_REGION = "us-east-1"
# The email subject
SUBJECT = "Notification Project"

def notify(event, context):
    # Retrieve bucket information
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    try:
        # Retrieve the file that invoked the function
        response = s3.get_object(Bucket=bucket, Key=key)
        file_content = response['Body'].read().decode('utf-8')
        # Load the file and output the values
        json_content = json.loads(file_content)
        print(json_content['phone'])
        print(json_content['email'])
        print(json_content['text'])
        # Send an Email using the email and text retrieved from the file
        sendEmail(json_content['email'], json_content['text'])
        # Send a SMS using the phone number and text retrieved from the file
        sendSMS(json_content['phone'], json_content['text'])
    except Exception as e:
        print(e)

def sendEmail(email, text):
    client = boto3.client('ses',region_name=AWS_REGION)
    try:
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    email,
                ],
            },
            Message={
                'Body': {
                    'Text': {
                        'Data': text
                    },
                },
                'Subject': {
                    'Data': SUBJECT
                },
            },
            Source=SENDER
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])

def sendSMS(phone, text):
    sns = boto3.client('sns',region_name=AWS_REGION)
    try:
        sns.publish(PhoneNumber=phone, Message=text )
    except ClientError as e:
        print(e.sns['Error']['Message'])
    else:
        print("Text sent! Message ID:"),
        print(sns['MessageId'])
