# Event Driven Notification Project  #

This project uses Terraform and AWS to send out SMS/Email Notifications

### What is this repository for? ###

* Stores json files with email, phone and message in S3
* Calls a function to retrieve the file and send out and SMS/Email with the retrieved message

### How do I get set up? ###

* Adjust the global properties in main.tf and notifications.py
* Validate your email address and phone number, required if you are using a sandbox account
* Terraform and AWS is required
* Configure the email address you would like to use
* Initiate terraform and apply the changes

### How do I use it? ###

* Run 'terraform init' and 'terraform apply'
* Upload a json file to S3, an example can be found in this project
